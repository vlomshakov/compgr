//
// Created by Vadim Lomshakov on 12/11/13.
// Copyright (c) 2013 WooHoo. All rights reserved.
//



#ifndef __ModelAsset_H_
#define __ModelAsset_H_

#include <GL/glew.h>
#include <vector>
#include <memory>
#include <glm/glm.hpp>
#include "Program.h"
#include "Texture.h"

struct LightSourceData;
struct ModelAsset {
  std::shared_ptr<Program> shaders;
  std::shared_ptr<Texture> texture;
  GLuint vbo;
  GLuint vao;
  GLuint ibo;
  GLenum drawType;
  GLint drawCount;
  GLfloat texCoordMultiplier;

  ModelAsset() :
    vbo(0),
    vao(0),
    ibo(0),
    drawType(GL_TRIANGLES),
    drawCount(0),
    texCoordMultiplier(1.0f)
  {}

  virtual void drawAsset(glm::mat4 const &model, glm::mat4 const &camera) = 0;

  virtual ~ModelAsset() {};
};

#endif //__ModelAsset_H_
