//
//  Shaders.h
//
//
//  Created by Vadim Lomshakov on 12/06/13.
//  Copyright (c) 2013 spbau. All rights reserved.
//

#ifndef test_Graphics_Shaders_h
#define test_Graphics_Shaders_h

#include "Shader.h"

////////////////////////////////////////////////////////////////////////////////

static const char* frg_sh = "\
#version 150 \n\
in vec2 textCoord0;\
\
out vec4 FragColor;\
\
uniform sampler2D gSampler; \
uniform float texMul;\
\
void main() {\
  FragColor = texture(gSampler, texMul * textCoord0);\
}";


static const char* vtx_sh = "\
#version 150 \n\
in vec3 position; \
in vec2 textCoord;  \
\
out vec2 textCoord0; \
\
uniform mat4 gWVP; \
\
void main() { \
\
gl_Position = gWVP * vec4(position, 1.0); \
textCoord0 = textCoord; \
}";



static const char* frg_sh1 = "\
#version 150 \n\
\
out vec4 FragColor;\
\
in vec3 pos;\
\
uniform sampler2D gSampler; \
uniform float texMul;\
\
void main() {\
  float PI = 3.14159;\
  float v = 1 - acos(pos.y) / PI;\
  float u = atan(pos.x , pos.z);\
  \
  vec2 texCoord = vec2(u/(2*PI), v);\
  FragColor = texture(gSampler, texMul * texCoord);\
}";

//  if (pos.x >= 0 && pos.z > 0)
//    u = atan(pos.x / pos.z) / (2*PI);
//  else if (pos.x < 0 && pos.z > 0)
//    u = atan(pos.x / pos.z) / (2*PI) + 1.0;
//  else if (pos.z < 0)
//    u = atan(pos.x / pos.z) / (2*PI) + 0.5;
//  else if (pos.z == 0 && pos.x < 0)
//    u = 0.75;
//  else if (pos.z == 0 && pos.x > 0)
//    u = 0.25;
//  else if (pos.x == 0 && pos.z == 0)
//    u = 0;

static const char* vtx_sh1 = "\
#version 150 \n\
in vec3 position; \
\
out vec3 pos;\
\
uniform mat4 gWVP; \
\
void main() { \
  pos = position;\
  \
  gl_Position = gWVP * vec4(position, 1.0); \
}";

#endif
