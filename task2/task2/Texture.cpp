//
// Created by Vadim Lomshakov on 06/12/13.
// Copyright (c) 2013 spbau. All rights reserved.
//


#include "Texture.h"


Texture *Texture::createTextureFromImage(std::string const &filename, GLint minMagFiler, GLint wrapMode) {
  std::shared_ptr<Magick::Image> image(new Magick::Image(filename));
  size_t u = image->columns();
  size_t v = image->rows();
  size_t _u = 2, _v = 2;
  // size image must be pow of 2
  if ((u & (u - 1)) != 0 || (v & (v - 1)) != 0) {
    while (u > _u) _u <<= 1;
    while (v > _v) _v <<= 1;
    image->resize(Magick::Geometry(_u, _v));
  }

  image->flip();
  return new Texture(image, minMagFiler, wrapMode);
}


Texture::Texture(std::shared_ptr<Magick::Image> image, GLint minMagFiler, GLint wrapMode)
  : _originalHeight(image->rows()), _originalWidth(image->columns())
{
  std::shared_ptr<Magick::Blob> blob(new Magick::Blob);
  image->write(blob.get(), "RGBA");

  glGenTextures(1, &_object);
  glBindTexture(GL_TEXTURE_2D, _object);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, minMagFiler);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, minMagFiler);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, wrapMode);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, wrapMode);
  glTexImage2D(GL_TEXTURE_2D,
    0, //mip-map level
    GL_RGB8,
    (GLsizei)image->columns(),
    (GLsizei)image->rows(),
    0, // border
    GL_RGBA,
    GL_UNSIGNED_BYTE,
    blob->data());
  glGenerateMipmap(GL_TEXTURE_2D);

  glBindTexture(GL_TEXTURE_2D, 0);
}

Texture::~Texture()
{
  glDeleteTextures(1, &_object);
}

GLuint Texture::object() const
{
  return _object;
}

GLfloat Texture::originalWidth() const
{
  return _originalWidth;
}

GLfloat Texture::originalHeight() const
{
  return _originalHeight;
}

void Texture::changeFilter(GLenum filter) {
  glBindTexture(GL_TEXTURE_2D, _object);

  switch (filter) {
    case GL_NEAREST:
      glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, filter);
      glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, filter);
      break;
    case GL_LINEAR:
      glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, filter);
      glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, filter);
      break;
    case GL_NEAREST_MIPMAP_NEAREST:
    case GL_NEAREST_MIPMAP_LINEAR:
    case GL_LINEAR_MIPMAP_NEAREST:
    case GL_LINEAR_MIPMAP_LINEAR:
      glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
      glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, filter);
      break;
    default:
      break;
  }

  glBindTexture(GL_TEXTURE_2D, 0);
}
