//
// Created by Vadim Lomshakov on 07/12/13.
// Copyright (c) 2013 spbau. All rights reserved.
//


#ifndef __SquareAsset_H_
#define __SquareAsset_H_

#include "ModelAsset.h"
#include "Shaders.h"
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>


struct SquareAsset: ModelAsset {
public:

  SquareAsset() {
    loadAsset();
  }

  void drawAsset(glm::mat4 const& model, glm::mat4 const &camera) {
    shaders->use();

    glBindVertexArray(vao);
    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D, texture->object());


    glUniformMatrix4fv(shaders->uniLc("gWVP"), 1.0f, GL_FALSE, glm::value_ptr(camera * model));
    glUniform1i(shaders->uniLc("gSampler"), 0); // set unit 0
    glUniform1f(shaders->uniLc("texMul"), texCoordMultiplier);

    glDrawElements(drawType, drawCount, GL_UNSIGNED_INT, 0);


    glBindVertexArray(0);
    glBindTexture(GL_TEXTURE_2D, 0);
    shaders->stopUsing();
  }

private:

  void loadAsset() {
    shaders.reset(new Program(vtx_sh, frg_sh));
    drawType = GL_TRIANGLES;
#ifdef _NO_IDE_
    texture.reset(Texture::createTextureFromImage("./lena.jpg"));
#else
    texture.reset(Texture::createTextureFromImage("/Users/vlomshakov/Documents/vadik/graphics/tasks/task2/task2/lena.jpg"));
#endif
    drawCount =  2*3;

    // orientation is CCW
    GLfloat vertexData[] = {
    //    X    Y    Z         U     V
        1.0f, -1.0f, .0f,    1.0f, 0.0f,
        1.0f, 1.0f, .0f,     1.0f, 1.0f,
       -1.0f, 1.0f, .0f,     0.0f, 1.0f,
       -1.0f, -1.0f, .0f,    0.0f, 0.0f,
    };

    GLint idxData[] = {
        0, 1 , 2,
        2, 3 , 0
    };

    glGenVertexArrays(1, &vao);
    glBindVertexArray(vao);

    glGenBuffers(1, &vbo);
    glBindBuffer(GL_ARRAY_BUFFER, vbo);
    glBufferData(GL_ARRAY_BUFFER, sizeof(vertexData), vertexData, GL_STATIC_DRAW);

    glGenBuffers(1, &ibo);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ibo);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(idxData),
        idxData, GL_STATIC_DRAW);

    glEnableVertexAttribArray(shaders->attLc("position"));
    glEnableVertexAttribArray(shaders->attLc("textCoord"));


    GLsizei stride = 5 * sizeof(GLfloat);
    glVertexAttribPointer(shaders->attLc("position"), 3, GL_FLOAT, GL_FALSE, stride, NULL);
    glVertexAttribPointer(shaders->attLc("textCoord"), 2, GL_FLOAT, GL_TRUE, stride, (const GLvoid *)(3 * sizeof(GLfloat)));


    glBindVertexArray(0);
  }
};


#endif //__SquareAsset_H_
