//
//  Texture.h
//
// Created by Vadim Lomshakov on 06/12/13.
// Copyright (c) 2013 spbau. All rights reserved.
//



#ifndef __Texture_H_
#define __Texture_H_

#include <GL/glew.h>
#include <string>
#include <memory>
#include <ImageMagick-6/Magick++/Image.h>


class Texture {
public:

  static Texture *createTextureFromImage(std::string const &filename,
      GLint minMagFiler = GL_LINEAR,
      GLint wrapMode = GL_REPEAT);

  Texture(std::shared_ptr<Magick::Image> image,
      GLint minMagFiler, GLint wrapMode);

  /**
   * Deletes the texture object with glDeleteTextures
   */
  ~Texture();

  /**
   * @return The texure object, as created by glGenTextures
   */
  GLuint object() const;
  GLfloat originalWidth() const;
  GLfloat originalHeight() const;

  void changeFilter(GLenum filter);
private:
  GLuint _object;
  GLfloat _originalHeight;
  GLfloat _originalWidth;


  Texture(const Texture&);
  const Texture& operator=(const Texture&);
};


#endif //__Texture_H_
