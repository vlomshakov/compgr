#ifndef LightingDemo_Shader_h
#define LightingDemo_Shader_h

#include <GL/glew.h>
#include <iostream>
#include <stdexcept>
#include <string>

class Shader {
public: // methods
  Shader(const char* shader_text, GLenum shader_type) {
    m_shader_id = glCreateShader(shader_type);
    
    if (m_shader_id == 0) {
      throw std::runtime_error("Error creating shader type " + std::to_string(shader_type));
    }
    
    GLint len = (GLint)strlen(shader_text);
    glShaderSource(m_shader_id, 1, &shader_text, &len);
    glCompileShader(m_shader_id);
    GLint success;
    glGetShaderiv(m_shader_id, GL_COMPILE_STATUS, &success);
    if (!success) {
      GLchar log[1024];
      glGetShaderInfoLog(m_shader_id, 1024, NULL, log);
      std::cerr << log << std::endl;
      glDeleteShader(m_shader_id);
      throw std::runtime_error("Error compiling shader type " + std::to_string(shader_type));
    }
  }
  
  ~Shader() { glDeleteShader(m_shader_id); }
  
  inline GLuint descr() const { return m_shader_id; }
  
private: // methods
  Shader(const Shader &);
  Shader & operator=(const Shader &);
private: // fields
  GLuint m_shader_id;
};

#endif
