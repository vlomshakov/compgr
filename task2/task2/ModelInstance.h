//
// Created by Vadim Lomshakov on 07/12/13.
// Copyright (c) 2013 spbau. All rights reserved.
//


#ifndef __ModelInstance_H_
#define __ModelInstance_H_

#include "ModelAsset.h"
#include <memory>


struct ModelInstance {
  glm::mat4 transform;
  std::shared_ptr<ModelAsset> asset;

  void draw(glm::mat4 const& camera, float gTexCoordMult) {
    asset->texCoordMultiplier = gTexCoordMult;
    asset->drawAsset(transform, camera);
  }

  ModelInstance() :
  transform(),
  asset(0)
  {}

  ModelInstance(ModelAsset* mAsset, glm::mat4 world) :
    transform(world),
    asset(mAsset)
  {}
};


#endif //__ModelInstance_H_
