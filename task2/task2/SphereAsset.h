//
// Created by Vadim Lomshakov on 07/12/13.
// Copyright (c) 2013 spbau. All rights reserved.
//



#ifndef __SphereAsset_H_
#define __SphereAsset_H_

#include <cmath>
#include <vector>
#include "ModelAsset.h"
#include "Shaders.h"
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <unordered_map>


struct SphereAsset: ModelAsset {
public:

  SphereAsset() {
    loadAsset();
  }

  void drawAsset(glm::mat4 const& model, glm::mat4 const &camera) {
//    glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
    shaders->use();

    glBindVertexArray(vao);
    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D, texture->object());


    glUniformMatrix4fv(shaders->uniLc("gWVP"), 1.0f, GL_FALSE, glm::value_ptr(camera * model));
    glUniform1i(shaders->uniLc("gSampler"), 0); // set unit 0
    glUniform1f(shaders->uniLc("texMul"), texCoordMultiplier);


    glDrawElements(drawType, drawCount, GL_UNSIGNED_INT, 0);


    glBindVertexArray(0);
    glBindTexture(GL_TEXTURE_2D, 0);
    shaders->stopUsing();
  }

private:

  void addAndNormalizePoint(std::vector<float> & vtx, glm::vec3 const &p) {
    float length = sqrt(p.x * p.x + p.y * p.y + p.z * p.z);
    vtx.push_back(p.x / length);
    vtx.push_back(p.y / length);
    vtx.push_back(p.z / length);
  }

  int getMiddlePoint(std::vector<float> & vtx, int i1, int i2, std::unordered_map<int64_t, int>& cache) {
    bool firstIsSmaller = i1 < i2;
    int smallerIndex = firstIsSmaller ? i1 : i2;
    int greaterIndex = firstIsSmaller ? i2 : i1;
    int64_t key = ((int64_t)smallerIndex << 32) + greaterIndex;

    if (cache.count(key) != 0) {
      return cache[key];
    }

    int newIdx = (int) vtx.size() /3;
    float x = (vtx[i1*3] + vtx[i2*3]) / 2.0f;
    float y = (vtx[i1*3 + 1] + vtx[i2*3 + 1]) / 2.0f;
    float z = (vtx[i1*3 + 2] + vtx[i2*3 + 2]) / 2.0f;
    float length = sqrt(x * x + y * y + z * z);
    vtx.push_back(x / length);
    vtx.push_back(y / length);
    vtx.push_back(z / length);

    return newIdx;
  }

  void generateSphere(std::vector<float>& vtx, std::vector<int>& idx, int countIter = 3) {
    idx = {
      0, 11, 5,
      0,  5,  1,
      0,  1,  7,
      0, 7, 10,
      0, 10, 11,

      1,  5, 9,
      5, 11,  4,
      11, 10,  2,
      10, 7,  6,
      7,  1,  8,

      3,  9, 4,
      3,  4,  2,
      3,  2,  6,
      3, 6,  8,
      3,  8,  9,

      4,  9, 5,
      2,  4, 11,
      6,  2, 10,
      8, 6,  7,
      9,  8, 1
    };

    float t = (1.0f + sqrt(5.0)) / 2.0f; // golden ratio

    // 12 vertices of a icosahedron
    addAndNormalizePoint(vtx, glm::vec3(-1, t, 0));
    addAndNormalizePoint(vtx, glm::vec3(1, t, 0));
    addAndNormalizePoint(vtx, glm::vec3(-1, -t, 0));
    addAndNormalizePoint(vtx, glm::vec3(1, -t, 0));

    addAndNormalizePoint(vtx, glm::vec3(0, -1, t));
    addAndNormalizePoint(vtx, glm::vec3(0, 1, t));
    addAndNormalizePoint(vtx, glm::vec3(0, -1, -t));
    addAndNormalizePoint(vtx, glm::vec3(0, 1, -t));

    addAndNormalizePoint(vtx, glm::vec3(t, 0, -1));
    addAndNormalizePoint(vtx, glm::vec3(t, 0, 1));
    addAndNormalizePoint(vtx, glm::vec3(-t, 0, -1));
    addAndNormalizePoint(vtx, glm::vec3(-t, 0, 1));


    // refine triangles
    for (int i = 0; i < countIter; i++)
    {
      std::unordered_map<int64_t, int> cacheForMiddlePoint;
      std::vector<int> idx2;      
      for (size_t face = 0; face < idx.size(); face += 3) {
        int oldA = idx[face];
        int oldB = idx[face + 1];
        int oldC = idx[face + 2];

        // replace triangle by 4 triangles
        int a = getMiddlePoint(vtx, oldA, oldB, cacheForMiddlePoint);
        int b = getMiddlePoint(vtx, oldB, oldC, cacheForMiddlePoint);
        int c = getMiddlePoint(vtx, oldC, oldA, cacheForMiddlePoint);

        idx2.push_back(oldA); idx2.push_back(a); idx2.push_back(c);
        idx2.push_back(oldB); idx2.push_back(b); idx2.push_back(a);
        idx2.push_back(oldC); idx2.push_back(c); idx2.push_back(b);
        idx2.push_back(a); idx2.push_back(b); idx2.push_back(c);
      }
      
      idx = idx2;
    }
  }

  void loadAsset() {
    shaders.reset(new Program(vtx_sh1, frg_sh1));
    drawType = GL_TRIANGLES;
#ifdef _NO_IDE_
    texture.reset(Texture::createTextureFromImage("./earth_texture_grid.bmp"));
#else
    texture.reset(Texture::createTextureFromImage("/Users/vlomshakov/Documents/vadik/graphics/tasks/task2/task2/earth_texture_grid.bmp"));
#endif
    std::vector<float> vertexData;
    std::vector<int> idxData;
    generateSphere(vertexData, idxData);

    drawCount = (GLint)idxData.size();

    glGenVertexArrays(1, &vao);
    glBindVertexArray(vao);

    glGenBuffers(1, &vbo);
    glBindBuffer(GL_ARRAY_BUFFER, vbo);
    glBufferData(GL_ARRAY_BUFFER, sizeof(GLfloat) * vertexData.size(), &vertexData[0], GL_STATIC_DRAW);

    glGenBuffers(1, &ibo);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ibo);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(GLint) * idxData.size(),
        &idxData[0], GL_STATIC_DRAW);

    glEnableVertexAttribArray(shaders->attLc("position"));

    GLsizei stride = 3 * sizeof(GLfloat);
    glVertexAttribPointer(shaders->attLc("position"), 3, GL_FLOAT, GL_FALSE, stride, NULL);


    glBindVertexArray(0);
  }
};


#endif //__SphereAsset_H_
