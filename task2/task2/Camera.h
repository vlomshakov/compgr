//
// Created by Vadim Lomshakov on 07/12/13.
// Copyright (c) 2013 spbau. All rights reserved.
//

#ifndef __Camera_H_
#define __Camera_H_

#include <GL/glew.h>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>



class Camera {
public:
  Camera();


  const glm::vec3& position() const;
  void setPosition(const glm::vec3& position);


  GLfloat fieldOfView() const;
  void setFieldOfView(GLfloat fieldOfView);


  GLfloat nearPlane() const;
  GLfloat farPlane() const;
  void setNearAndFarPlanes(GLfloat nearPlane, GLfloat farPlane);


  GLfloat pitch() const;
  void setPitch(GLfloat pitch);

  GLfloat heading() const;
  void setHeading(GLfloat heading);


  GLfloat viewportAspectRatio() const;
  void setViewportAspectRatio(GLfloat viewportAspectRatio);

  /**
   The combined camera transformation matrix, including perspective projection.
   */
  glm::mat4 matrix() const;

private:
  glm::vec3 _position;
  GLfloat _viewportAspectRatio;
  GLfloat _fov;
  GLfloat _heading;
  GLfloat _pitch;
  GLfloat _zNear;
  GLfloat _zFar;

};



#endif //__Camera_H_
