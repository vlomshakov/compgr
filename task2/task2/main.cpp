//
//  main.cpp
//  task2
//
//  Created by Vadim Lomshakov on 06/12/13.
//  Copyright (c) 2013 spbau. All rights reserved.
//

#include <iostream>
#include <GL/glew.h>
#include <GLFW/glfw3.h>

#include <ImageMagick-6/Magick++.h>
#include <vector>
#include <cmath>

#include <stdexcept>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include "ModelInstance.h"
#include "SquareAsset.h"
#include "Camera.h"
#include "CubeAsset.h"
#include "SphereAsset.h"

static void onKey(GLFWwindow *window, int key, int ,int , int );
static void onMouse(GLFWwindow* window, double x, double y);
static void onMouseBut(GLFWwindow* window, int button, int state, int);
static void onResize(GLFWwindow* window, int width, int height);

// globals
std::vector<ModelInstance> gInstances;
int currentModel = 2;
Camera gCamera;
float gTexCoordMult = 1.0f;



void initCallbacks(GLFWwindow* window) {
  glfwSetMouseButtonCallback(window, onMouseBut);
  glfwSetKeyCallback(window, onKey);
  glfwSetWindowSizeCallback(window, onResize);
}

static void onResize(GLFWwindow* window, int width, int height) {
  gCamera.setViewportAspectRatio((float)width / height);
}

static void onKey(GLFWwindow *window, int key, int ,int , int ) {
  static GLenum filters[] = {
      GL_NEAREST,                 // key 1
      GL_LINEAR,                  // 2
      GL_NEAREST_MIPMAP_NEAREST,  // 3
      GL_NEAREST_MIPMAP_LINEAR,   // 4
      GL_LINEAR_MIPMAP_NEAREST,   // 5
      GL_LINEAR_MIPMAP_LINEAR     // 6
  };

  switch (key) {
    case GLFW_KEY_S:
      gCamera.setFieldOfView(gCamera.fieldOfView() - 1.0f);
      break;
    case GLFW_KEY_W:
      gCamera.setFieldOfView(gCamera.fieldOfView() + 1.0f);
      break;
    case GLFW_KEY_D:
      gCamera.setNearAndFarPlanes(gCamera.nearPlane() - 1.0f, gCamera.farPlane());
      break;
    case GLFW_KEY_E:
      gCamera.setNearAndFarPlanes(gCamera.nearPlane() + 1.0f, gCamera.farPlane());
      break;
    case GLFW_KEY_F:
      gCamera.setNearAndFarPlanes(gCamera.nearPlane() , gCamera.farPlane() - 1.0f);
      break;
    case GLFW_KEY_R:
      gCamera.setNearAndFarPlanes(gCamera.nearPlane(), gCamera.farPlane() + 1.0f);
      break;
    case GLFW_KEY_T:
      gTexCoordMult += 0.1f;
      break;
    case GLFW_KEY_G:
      gTexCoordMult -= 0.1f;
      break;
    case GLFW_KEY_1:
    case GLFW_KEY_2:
    case GLFW_KEY_3:
    case GLFW_KEY_4:
    case GLFW_KEY_5:
    case GLFW_KEY_6:
      gInstances[currentModel].asset->texture->changeFilter(filters[key - GLFW_KEY_0 - 1]);
      break;
    case GLFW_KEY_Y:
      currentModel = 0;
      break;
    case GLFW_KEY_H:
      currentModel = 1;
      break;
    case GLFW_KEY_N:
      currentModel = 2;
      break;
  }
}

static void onMouseBut(GLFWwindow* window, int button, int state, int) {
  if (button == GLFW_MOUSE_BUTTON_LEFT) {
    if (state == GLFW_PRESS) {
      glfwSetCursorPosCallback(window, onMouse);
    } else {
      glfwSetCursorPosCallback(window, 0);
    }
  }
}

static void onMouse(GLFWwindow* window, double x, double y) {
  static int _prevX = 0;
  static int _prevY = 0;
  int diffX = (int) (x - _prevX);
  int diffY = (int) (y - _prevY);

  if (diffX > 0)
    gCamera.setHeading(gCamera.heading() - 1);
  if (diffX < 0)
    gCamera.setHeading(gCamera.heading() + 1);
  _prevX = (int) x;

  if (diffY > 0)
    gCamera.setPitch(gCamera.pitch() - 1);
  if (diffY < 0)
    gCamera.setPitch(gCamera.pitch() + 1);
  _prevY = (int) y;

}


static void createInstances() {
  ModelInstance square(new SquareAsset(), glm::scale(glm::mat4(), glm::vec3(10, 10, 1)));
  gInstances.push_back(square);
  ModelInstance cube(new CubeAsset(), glm::scale(glm::mat4(), glm::vec3(5, 5, 5)));
  gInstances.push_back(cube);
  ModelInstance sphere(new SphereAsset(), glm::scale(glm::mat4(), glm::vec3(10, 10, 10)));
  gInstances.push_back(sphere);
}

static void render(GLFWwindow* window) {
  glClearColor(0, 0, 0, 1);
  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

  assert(currentModel < gInstances.size() && currentModel >= 0);
  gInstances[currentModel].draw(gCamera.matrix(), gTexCoordMult);

  glfwSwapBuffers(window);
}


int main(int argc, const char * argv[])
{
  // initialise Magick++
  Magick::InitializeMagick(*argv);
  // initialise GLFW
  if(!glfwInit()) { throw std::runtime_error("glfwInit failed"); }

  // open a window with GLFW
  glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
  glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 2);
  glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
  glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);


  GLFWwindow* window = glfwCreateWindow(800, 600, "Texturing task", NULL, NULL);
  glfwMakeContextCurrent(window);

  glewExperimental = GL_TRUE;
  if(glewInit() != GLEW_OK) { throw std::runtime_error("glewInit failed"); }
  // GLEW throws some errors, so discard all the errors so far
  while(glGetError() != GL_NO_ERROR) {}

  createInstances();
  initCallbacks(window);

  // init common state
  glEnable(GL_DEPTH_TEST);
  glDepthFunc(GL_LESS);

  while (!glfwWindowShouldClose(window)) {

    render(window);

    // check for errors
    GLenum error = glGetError();
    if(error != GL_NO_ERROR)
      std::cerr << "OpenGL Error " << error << ": " << (const char*)gluErrorString(error) << std::endl;

    glfwWaitEvents();
  }


  glfwDestroyWindow(window);
  glfwTerminate();

  return 0;
}
