deps: glew, glfw3, opengl3.2/glsl1.5 , glm, ImageMagick++-6



for run:
        
        $cd ./task1
        $make
        $./program


keys:
    s/w - decrease/increase fov

    d/e - decrease/increase zFar

    f/r - decrease/increase zNear

    t/g - decrease/increase texture multiplier

    y/h/n -  rendering plane / cube / sphere 

    1, 2, 3, 4, 5, 5, 6 - nearest, linear, nearest_mipmap_nearest, nearest_mipmap_linear, linear_mipmap_nearest, linear_mipmap_linear

