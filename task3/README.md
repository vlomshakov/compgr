deps: glew, freeglut, opengl3.2/glsl1.2 , glm, anttweakbar

for run:
        
        $cd ./task3
        $make run


keys:
    w/s - decrease/increase fov
    e/d - decrease/increase zFar
    r/f -  decrease/increase zNear
    
    use bar and right button popup menu

