//
// Created by Vadim Lomshakov on 24/12/13.
// Copyright (c) 2013 spbau. All rights reserved.
//


#ifndef __ConesModel_H_
#define __ConesModel_H_


#include <string>
#include <cmath>
#include <vector>
#include "ModelAsset.h"
#include "Shaders.h"
#include "ObjFileLoader.h"
#include "LightSourceData.h"
#include "ModelInstance.h"
#include "Camera.h"
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>


class ConesModel : ModelInstance {
  GLfloat height;
  LightSourceData l;
  float maxDenominator; // into 1 / (1 + coeffAttent*dist^2)
  int cntTriangle;
public:

  ConesModel()
    : height(0),
    cntTriangle(0)
  {
  }

  ConesModel(glm::mat4 transformMat,LightSourceData const & pl, float maxDenom = 225.0f) :
    ModelInstance(transformMat),
    height(sqrtf((maxDenom - 1) / pl.attenuation)),
    l(pl),
    maxDenominator(maxDenom),
    cntTriangle(20)
  {
    loadConesAsset();
  }

  void draw(Camera const &camera, LightSourceData const &lightSrcData, MaterialData const &material) {
    glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
    glm::mat4 model = transformModel;
    setNewLightData(lightSrcData);

    asset.shaders->use();

    glBindBuffer(GL_ARRAY_BUFFER, asset.vbo);

    glEnableVertexAttribArray(asset.shaders->attLc("vert"));

    GLsizei stride = 3 * sizeof(GLfloat);
    glVertexAttribPointer(asset.shaders->attLc("vert"), 3, GL_FLOAT, GL_FALSE, stride, NULL);

    glUniformMatrix4fv(asset.shaders->uniLc("camera"), 1.0f, GL_FALSE, glm::value_ptr(camera.projectionView()));
    glUniformMatrix4fv(asset.shaders->uniLc("model"), 1.0f, GL_FALSE, glm::value_ptr(model));
    glDrawArrays(asset.drawType, 0, asset.drawCount);


    glDisableVertexAttribArray(asset.shaders->attLc("vert"));

    glBindBuffer(GL_ARRAY_BUFFER, 0);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);

    asset.shaders->stopUsing();
  }

  void setShadingPerFrg() {}
  void setShadingPerVtx() {}
  void setShadingPerFct() {}
  void setPhongFunc() {}
  void setBlinPhongFunc() {}
  void setShadingAndFuncType(TYPE_SHADING shadingType, TYPE_FUNC funcType) {}

  ~ConesModel() {}
private:
  void setNewLightData(LightSourceData const & pl) {
    height = sqrtf((maxDenominator - 1) / pl.attenuation);
    l = pl;
    updateMesh();
  }

  void updateMesh() {
    std::vector<float> data = genCone();

    glBindBuffer(GL_ARRAY_BUFFER, asset.vbo);
    glBufferData(GL_ARRAY_BUFFER, sizeof(float) * data.size(), &data[0], GL_STATIC_DRAW);

    glBindBuffer(GL_ARRAY_BUFFER, 0);
  }

  std::vector<float> genCone() {
    assert(cntTriangle > 0);
    float edgeCone = height / l.cutoffCos;
    float rCone = sqrtf(edgeCone * edgeCone - height * height); // radius of cone

    glm::vec3 P2 = l.position + height * glm::normalize(l.direction); // center of cone base

    // basis
    glm::vec3 A = glm::cross(glm::normalize(-l.direction), glm::vec3(0,0,1));
    if (A == glm::vec3(0,0,0)) {
      A = glm::cross(glm::normalize(-l.direction), glm::vec3(0,1,0));
    }
    if (A == glm::vec3(0,0,0)) {
      A = glm::cross(glm::normalize(-l.direction), glm::vec3(1,0,0));
    }
    A = glm::normalize(A);
    glm::vec3 B = glm::normalize(glm::cross(A, glm::normalize(-l.direction)));

    std::vector<float> cone(cntTriangle * 3 * 3);
    double theta1, theta2;
    const double TWO_PI = M_PI * 2.0;
    int n = 0;
    for (int i = 0; i < cntTriangle; i++) {
      theta1 = i * TWO_PI / cntTriangle;
      theta2 = (i + 1) * TWO_PI / cntTriangle;
      cone[n]   = l.position.x;
      cone[n+1] = l.position.y;
      cone[n+2] = l.position.z;
      n += 3;
      cone[n]   = (float) (P2.x + rCone * cos(theta1) * A.x + rCone * sin(theta1) * B.x);
      cone[n+1] = (float) (P2.y + rCone * cos(theta1) * A.y + rCone * sin(theta1) * B.y);
      cone[n+2] = (float) (P2.z + rCone * cos(theta1) * A.z + rCone * sin(theta1) * B.z);
      n += 3;
      cone[n]   = (float) (P2.x + rCone * cos(theta2) * A.x + rCone * sin(theta2) * B.x);
      cone[n+1] = (float) (P2.y + rCone * cos(theta2) * A.y + rCone * sin(theta2) * B.y);
      cone[n+2] = (float) (P2.z + rCone * cos(theta2) * A.z + rCone * sin(theta2) * B.z);
      n += 3;
    }

    return cone;
  }

  void loadConesAsset() {
    asset.shaders.reset(new Program(vtx_sh_cone, frg_sh_cone));
    asset.drawType = GL_TRIANGLES;

    asset.drawCount = cntTriangle*3*3;

    std::vector<float> data = genCone();

    glGenBuffers(1, &asset.vbo);
    glBindBuffer(GL_ARRAY_BUFFER, asset.vbo);
    glBufferData(GL_ARRAY_BUFFER, sizeof(float) * data.size(), &data[0], GL_STATIC_DRAW);

    glBindBuffer(GL_ARRAY_BUFFER, 0);
  }
};



#endif //__ConesModel_H_
