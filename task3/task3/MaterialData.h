//
// Created by Vadim Lomshakov on 23/12/13.
// Copyright (c) 2013 spbau. All rights reserved.
//


#ifndef __MaterialData_H_
#define __MaterialData_H_

#include <glm/glm.hpp>
#include <GL/glew.h>

struct MaterialData {
  GLfloat shininess;
  glm::vec3 specularColor;
  GLfloat ambientCoefficient;
  glm::vec3 ambientColor;
  glm::vec4 diffuseColor;
};


#endif //__MaterialData_H_
