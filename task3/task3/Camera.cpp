//
// Created by Vadim Lomshakov on 07/12/13.
// Copyright (c) 2013 spbau. All rights reserved.
//


#include "Camera.h"



Camera::Camera() :
  _position(0, 0, 30),
  _viewportAspectRatio(800.0f / 600.0f),
  _fov(50.0f),
  _heading(0.0f),
  _pitch(0.0f),
  _zNear(0.1f),
  _zFar(40.0f)
{}

glm::mat4 Camera::matrix() const {
  glm::mat4 projectionMat = glm::perspective(_fov, _viewportAspectRatio , _zNear, _zFar);

  glm::mat4 viewMat = glm::lookAt(_position, glm::vec3(0, 0, 0), glm::vec3(0, 1, 0));

  glm::mat4 rotationYViewMat = glm::rotate(glm::mat4(1.0f), -_heading, glm::vec3(0, 1, 0));

  glm::mat4 rotationXViewMat = glm::rotate(glm::mat4(1.0f), -_pitch, glm::vec3(1, 0, 0));

  return projectionMat * viewMat * rotationYViewMat * rotationXViewMat;
}

glm::mat4 Camera::projectionView() const {
  glm::mat4 projectionMat = glm::perspective(_fov, _viewportAspectRatio , _zNear, _zFar);

  glm::mat4 viewMat = glm::lookAt(_position, glm::vec3(0, 0, 0), glm::vec3(0, 1, 0));

  return projectionMat * viewMat;
}

glm::mat4 Camera::model() const {
  glm::mat4 rotationYViewMat = glm::rotate(glm::mat4(1.0f), -_heading, glm::vec3(0, 1, 0));

  glm::mat4 rotationXViewMat = glm::rotate(glm::mat4(1.0f), -_pitch, glm::vec3(1, 0, 0));

  return rotationYViewMat * rotationXViewMat;
}

glm::mat4 Camera::modelQuat() const {
  return glm::mat4_cast(_camOrient);
}

GLfloat Camera::viewportAspectRatio() const {
  return _viewportAspectRatio;
}

void Camera::setViewportAspectRatio(GLfloat viewportAspectRatio) {
  _viewportAspectRatio = viewportAspectRatio;
}


GLfloat Camera::heading() const {
  return _heading;
}

void Camera::setHeading(GLfloat heading) {
  _heading = heading;
}


void Camera::setPitch(GLfloat pitch) {
  _pitch = pitch;
}

GLfloat Camera::pitch() const {
  return _pitch;
}

void Camera::setNearAndFarPlanes(GLfloat nearPlane, GLfloat farPlane) {
  assert(nearPlane > 0.0f);
  assert(farPlane > nearPlane);
  _zNear = nearPlane;
  _zFar = farPlane;
}

GLfloat Camera::farPlane() const {
  return _zFar;
}

GLfloat Camera::nearPlane() const {
  return _zNear;
}


void Camera::setFieldOfView(GLfloat fieldOfView) {
  _fov = fieldOfView;
}


GLfloat Camera::fieldOfView() const {
  return _fov;
}

void Camera::setPosition(const glm::vec3 &position) {
  _position = position;
}

const glm::vec3 &Camera::position() const {
  return _position;
}

void Camera::setCamOrient(glm::quat const &orient) {
  _camOrient = orient;
}

glm::quat Camera::camOrient() const {
  return _camOrient;
}
