//
//  ObjFileLoader.cpp
//  test_Graphics
//
//  Created by Hatless Fox on 10/12/13.
//  Copyright (c) 2013 WooHoo. All rights reserved.
//

#include "ObjFileLoader.h"
#include <fstream>
#include <sstream>
#include <stdexcept>

void ObjFileLoader::loadFromFile(std::string const & file_name) {
	std::ifstream obj_file;
	obj_file.open(file_name);
	if (!obj_file.is_open()) {
		throw std::runtime_error("unable to open file " + file_name);
	}
	while (obj_file.peek() != EOF) {
		std::string data;
		std::getline(obj_file, data);
		//TODO trim whitespaces
		
		std::stringstream ss(data);
		std::string cmd;
		ss >> cmd;
		
		if (cmd == "f") {
			handle_face(ss);
		} else if (cmd == "v") {
			handle_vtx(ss);
		} else if (cmd == "vn") {
      handle_nvtx(ss);
    }
	}
}

void ObjFileLoader::handle_nvtx(std::stringstream &data) {
  float vtx_normal_comp;
	for (int i = 0; i < 3; ++i) {
		data >> vtx_normal_comp;
		vtx_normal_data.push_back(vtx_normal_comp);
	}
}

void ObjFileLoader::handle_vtx(std::stringstream &ss) {
	float vtx_coord_comp;
	for (int i = 0; i < 3; ++i) {
		ss >> vtx_coord_comp;
		vtx_coord_data.push_back(vtx_coord_comp);
	}
}


void ObjFileLoader::handle_face(std::stringstream &ss) {
	std::string face_elem;
	for (int i = 0; i < 3; ++i) {
		ss >> face_elem;
		const char * str = face_elem.c_str();
		std::string value;
		while (*str && *str != '/') {	value += *str++;}
		int vtx_coord = std::stoi(value);
		if (vtx_coord < 0) {
			vtx_coord = ((int) vtx_coord_data.size() / 3) + vtx_coord;
		}
		// face ref is one based
		faces_data.push_back(vtx_coord - 1);
	}
}

size_t ObjFileLoader::loaded_vtx_coords_size() { return sizeof(float) * vtx_coord_data.size(); }
float * ObjFileLoader::loaded_vtx_coords() { return &vtx_coord_data[0]; }

size_t ObjFileLoader::loaded_vtx_normals_size() { return sizeof(float) * vtx_normal_data.size(); }
float * ObjFileLoader::loaded_vtx_normals() { return &vtx_normal_data[0];}

size_t ObjFileLoader::loaded_faces_size() { return sizeof(int) * faces_data.size();}
size_t ObjFileLoader::elems_cnt() { return faces_data.size(); }
int * ObjFileLoader::loaded_faces() { return &faces_data[0]; }

