//
// Created by Vadim Lomshakov on 07/12/13.
// Copyright (c) 2013 spbau. All rights reserved.
//



#ifndef __SphereAsset_H_
#define __SphereAsset_H_

#include <string>
#include <cmath>
#include <vector>
#include "ModelAsset.h"
#include "Shaders.h"
#include "ObjFileLoader.h"
#include "LightSourceData.h"
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <unordered_map>


class BunnyModel : ModelInstance {
  TYPE_SHADING typeShading;
  TYPE_FUNC typeFunc;
public:

  BunnyModel() :
    typeShading(TS_PER_FRG),
    typeFunc(TF_PHONG)
  {
    loadBunnyAsset();
  }

  BunnyModel(glm::mat4 transformMat) :
    ModelInstance(transformMat),
    typeShading(TS_PER_FRG),
    typeFunc(TF_PHONG)
  {
    loadBunnyAsset();
  }

  void draw(Camera const& camera, LightSourceData const & lightSrcData, MaterialData const & material) {
    glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
    glm::mat4 model = camera.modelQuat() * transformModel ;
    glm::mat3 normalMatrix = glm::transpose(glm::inverse(glm::mat3(model)));
    setNewMaterial(material);
    asset.shaders->use();

    glBindBuffer(GL_ARRAY_BUFFER, asset.vbo);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, asset.ibo);

    glEnableVertexAttribArray(asset.shaders->attLc("vert"));
    glEnableVertexAttribArray(asset.shaders->attLc("vertNormal"));

    GLsizei stride = 6 * sizeof(GLfloat);
    glVertexAttribPointer(asset.shaders->attLc("vert"), 3, GL_FLOAT, GL_FALSE, stride, NULL);
    glVertexAttribPointer(asset.shaders->attLc("vertNormal"), 3, GL_FLOAT, GL_TRUE, stride, (const GLvoid *)(3*sizeof(GLfloat)));


    glUniform3fv(asset.shaders->uniLc("light.color"), 1, glm::value_ptr(lightSrcData.color));
    glUniform3fv(asset.shaders->uniLc("light.position"), 1, glm::value_ptr(lightSrcData.position));
    glUniform3fv(asset.shaders->uniLc("light.direction"), 1, glm::value_ptr(lightSrcData.direction));
    glUniform1f(asset.shaders->uniLc("light.attenuation"), lightSrcData.attenuation);
    glUniform1f(asset.shaders->uniLc("light.cutoffCos"), lightSrcData.cutoffCos);
    glUniform1f(asset.shaders->uniLc("light.spotExponent"), lightSrcData.spotExponent);

    glUniform1f(asset.shaders->uniLc("material.shininess"), asset.material.shininess);
    glUniform3fv(asset.shaders->uniLc("material.specularColor"),1, glm::value_ptr(asset.material.specularColor));
    glUniform1f(asset.shaders->uniLc("material.ambientCoefficient"), asset.material.ambientCoefficient);
    glUniform3fv(asset.shaders->uniLc("material.ambientColor"),1, glm::value_ptr(asset.material.ambientColor));
    glUniform4fv(asset.shaders->uniLc("material.diffuseColor"),1, glm::value_ptr(asset.material.diffuseColor));

    glUniform3fv(asset.shaders->uniLc("cameraPosition"),1, glm::value_ptr(camera.position()));
    glUniformMatrix4fv(asset.shaders->uniLc("camera"), 1.0f, GL_FALSE, glm::value_ptr(camera.projectionView()));
    glUniformMatrix4fv(asset.shaders->uniLc("model"), 1.0f, GL_FALSE, glm::value_ptr(model));
    glUniformMatrix3fv(asset.shaders->uniLc("normalMatrix"), 1.0f, GL_FALSE, glm::value_ptr(normalMatrix));
    glDrawElements(asset.drawType, asset.drawCount, GL_UNSIGNED_INT, 0);


    glDisableVertexAttribArray(asset.shaders->attLc("vert"));
    glDisableVertexAttribArray(asset.shaders->attLc("vertNormal"));

    glBindBuffer(GL_ARRAY_BUFFER, 0);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);

    asset.shaders->stopUsing();
  }

  void setShadingPerFrg() { typeShading = TS_PER_FRG; updateShaders(); }

  void setShadingPerVtx() { typeShading = TS_PER_VTX; updateShaders(); }

  void setShadingPerFct() { typeShading = TS_PER_FCT; updateShaders(); }

  void setPhongFunc() { typeFunc = TF_PHONG; updateShaders(); }

  void setBlinPhongFunc() { typeFunc = TF_BLIN_PHONG; updateShaders(); }

  void setShadingAndFuncType(TYPE_SHADING shadingType, TYPE_FUNC funcType) {
    typeShading = shadingType;
    typeFunc = funcType;
    updateShaders();
  }

private:

  ~BunnyModel() {}
private:
  void updateShaders() {
    std::pair<std::string, std::string>  shaders = buildShaders(typeFunc, typeShading);
    asset.shaders.reset(new Program(shaders.first.c_str(), shaders.second.c_str()));
  }

  void loadBunnyAsset() {
    std::pair<std::string, std::string>  shaders = buildShaders(typeFunc, typeShading);
    asset.shaders.reset(new Program(shaders.first.c_str(), shaders.second.c_str()));
    asset.drawType = GL_TRIANGLES;
    asset.material.shininess = 20.0f;
    asset.material.specularColor = glm::vec3(1, 1, 1);
    asset.material.ambientCoefficient = 0.02f;
    asset.material.ambientColor = glm::vec3(0.7f, 0.0f, 0.0f);
    asset.material.diffuseColor = glm::vec4(0.7f, 0.0f, 0.0f, 1.0f);

    ObjFileLoader ldr;

#ifdef _NO_IDE_
    ldr.loadFromFile("./bunny_n.obj");
#else
    ldr.loadFromFile("/Users/vlomshakov/Documents/vadik/graphics/tasks/task3/task3/bunny_n.obj");
#endif

    //merge coords with normals
    assert(ldr.loaded_vtx_coords_size() == ldr.loaded_vtx_normals_size());

    std::vector<float> merged_data;
    float *coords = ldr.loaded_vtx_coords();
    float *normarls = ldr.loaded_vtx_normals();
    for (size_t i = 0; i < ldr.loaded_vtx_coords_size() / sizeof(float); i += 3) {
      for (size_t j = 0; j < 3; ++j) { merged_data.push_back(10 * (*coords++)); }
      for (size_t j = 0; j < 3; ++j) { merged_data.push_back(*normarls++); }
    }

    glGenBuffers(1, &asset.vbo);
    glBindBuffer(GL_ARRAY_BUFFER, asset.vbo);
    glBufferData(GL_ARRAY_BUFFER, sizeof(float) * merged_data.size(),
        &merged_data[0], GL_STATIC_DRAW);

    glGenBuffers(1, &asset.ibo);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, asset.ibo);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, ldr.loaded_faces_size(),
        ldr.loaded_faces(), GL_STATIC_DRAW);

    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
    glBindBuffer(GL_ARRAY_BUFFER, 0);

    asset.drawCount = (GLint)ldr.elems_cnt();
  }
};

#endif //__SphereAsset_H_
