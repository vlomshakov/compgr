//
// Created by Vadim Lomshakov on 23/12/13.
// Copyright (c) 2013 spbau. All rights reserved.
//


#ifndef __LightSourceData_H_
#define __LightSourceData_H_
#include <glm/glm.hpp>

struct LightSourceData {
  glm::vec3 color;
  glm::vec3 position;
  glm::vec3 direction;
  float attenuation;
  float cutoffCos;
  float spotExponent;

  LightSourceData() :
    attenuation(0),
    cutoffCos(0),
    spotExponent(0)
  {}

  LightSourceData(const glm::vec3 &position, const glm::vec3 &color, float attenuation,
      float cutoffCos, glm::vec3 direction, float spotExponent = 1.0f) :
    color(color),
    position(position),
    direction(direction),
    attenuation(attenuation),
    cutoffCos(cutoffCos),
    spotExponent(spotExponent) {

  }
};


#endif //__LightSourceData_H_
