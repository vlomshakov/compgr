//
// Created by Vadim Lomshakov on 07/12/13.
// Copyright (c) 2013 spbau. All rights reserved.
//

#ifndef __Camera_H_
#define __Camera_H_

#include <AntTweakBar.h>
#include <GL/glew.h>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>



class Camera {
public:
  Camera();


  const glm::vec3& position() const;
  void setPosition(const glm::vec3& position);

  GLfloat fieldOfView() const;
  void setFieldOfView(GLfloat fieldOfView);


  GLfloat nearPlane() const;
  GLfloat farPlane() const;
  void setNearAndFarPlanes(GLfloat nearPlane, GLfloat farPlane);


  GLfloat pitch() const;
  void setPitch(GLfloat pitch);

  GLfloat heading() const;
  void setHeading(GLfloat heading);

  void setCamOrient(glm::quat const & orient);
  glm::quat camOrient() const;

  GLfloat viewportAspectRatio() const;
  void setViewportAspectRatio(GLfloat viewportAspectRatio);

  /**
   The combined camera transformation matrix, including perspective projection and view.
   */
  glm::mat4 matrix() const;

  glm::mat4 model() const;
  glm::mat4 modelQuat() const;

  glm::mat4 projectionView() const;


  static void TW_CALL setFovCallback(const void *value, void *clientData) {
    static_cast<Camera *>(clientData)->setFieldOfView(*static_cast<const float *>(value));
  }

  static void TW_CALL getFovCallback(void *value, void *clientData) {
    *static_cast<float *>(value) = static_cast<Camera *>(clientData)->fieldOfView();
  }

  static void TW_CALL setNearCallback(const void *value, void *clientData) {
    Camera* cmr = static_cast<Camera *>(clientData);
    cmr->setNearAndFarPlanes(*static_cast<const float *>(value), cmr->farPlane());
  }

  static void TW_CALL getNearCallback(void *value, void *clientData) {
    *static_cast<float *>(value) = static_cast<Camera *>(clientData)->nearPlane();
  }

  static void TW_CALL setFarCallback(const void *value, void *clientData) {
    Camera* cmr = static_cast<Camera *>(clientData);
    cmr->setNearAndFarPlanes(cmr->nearPlane(), *static_cast<const float *>(value));
  }

  static void TW_CALL getFarCallback(void *value, void *clientData) {
    *static_cast<float *>(value) = static_cast<Camera *>(clientData)->farPlane();
  }

  static void TW_CALL setCamOrientCallback(const void *value, void *clientData) {
    static_cast<Camera *>(clientData)->setCamOrient(*static_cast<glm::quat const *>(value));
  }

  static void TW_CALL getCamOrientCallback(void *value, void *clientData) {
    *static_cast<glm::quat  *>(value) = static_cast<Camera *>(clientData)->camOrient();
  }

private:
  glm::vec3 _position;
  GLfloat _viewportAspectRatio;
  GLfloat _fov;
  GLfloat _heading;
  GLfloat _pitch;
  GLfloat _zNear;
  GLfloat _zFar;

  glm::quat _camOrient;
};



#endif //__Camera_H_
