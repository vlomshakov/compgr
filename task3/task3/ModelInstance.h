//
// Created by Vadim Lomshakov on 07/12/13.
// Copyright (c) 2013 spbau. All rights reserved.
//


#ifndef __ModelInstance_H_
#define __ModelInstance_H_

#include "ModelAsset.h"
#include "Camera.h"
#include "LightSourceData.h"
#include "Shaders.h"
#include <memory>

class Camera;
struct LightSourceData;
class ModelInstance {
protected:
  glm::mat4 transformModel;
  ModelAsset asset;
public:

  ModelInstance() :
  transformModel(), asset() {}

  ModelInstance(glm::mat4 transformMat) :
    transformModel(transformMat), asset() {}

  void setNewMaterial(MaterialData const& material) { asset.material = material; }

  virtual void setPhongFunc() = 0;
  virtual void setBlinPhongFunc() = 0;

  virtual void setShadingPerFrg() = 0;
  virtual void setShadingPerVtx() = 0;
  virtual void setShadingPerFct() = 0;

  virtual void setShadingAndFuncType(TYPE_SHADING shadingType, TYPE_FUNC funcType) = 0;

  virtual void draw(Camera const &camera, LightSourceData const &lightSrcData, MaterialData const &material) = 0;
  virtual ~ModelInstance() {};
};


#endif //__ModelInstance_H_
