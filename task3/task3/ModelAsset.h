//
// Created by Vadim Lomshakov on 12/11/13.
// Copyright (c) 2013 WooHoo. All rights reserved.
//



#ifndef __ModelAsset_H_
#define __ModelAsset_H_

#include <GL/glew.h>
#include <vector>
#include <memory>
#include "Program.h"
#include "MaterialData.h"

struct ModelAsset {
  std::shared_ptr<Program> shaders;
  GLuint vbo;
  GLuint ibo;
  GLenum drawType;
  GLint drawCount;

  MaterialData material;

public:
  ModelAsset() :
    vbo(0),
    ibo(0),
    drawType(GL_TRIANGLES),
    drawCount(0) {}
};

#endif //__ModelAsset_H_
