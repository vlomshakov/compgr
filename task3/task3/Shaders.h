//
//  Shaders.h
//
//
//  Created by Vadim Lomshakov.
//  Copyright (c) 2013 spbau. All rights reserved.
//

#ifndef test_Graphics_Shaders_h
#define test_Graphics_Shaders_h

#include "Shader.h"
#include <string>

////////////////////////////////////////////////////////////////////////////////

static const char* vtx_sh_per_frag = "#version 120\n"
                            "\n"
                            "uniform mat4 camera;\n"
                            "uniform mat4 model;\n"
                            "\n"
                            "attribute vec3 vert;\n"
                            "attribute vec3 vertNormal;\n"
                            "\n"
                            "varying vec3 fragVert;\n"
                            "varying vec3 fragNormal;\n"
                            "\n"
                            "void main() {\n"
                            "    fragNormal = vertNormal;\n"
                            "    fragVert = vert;\n"
                            "    \n"
                            "    gl_Position = camera * model * vec4(vert, 1);\n"
                            "}";

static const char* frg_sh_header_per_frag = "#version 120\n"
                            "\n"
                            "varying vec3 fragNormal;\n"
                            "varying vec3 fragVert;\n"
                            "\n"
                            "uniform mat4 model;\n"
                            "uniform mat3 normalMatrix;\n"
                            "uniform vec3 cameraPosition;\n"
                            "\n"
                            "uniform struct Material {\n"
                            "   float shininess;\n"
                            "   vec3 specularColor;\n"
                            "   float ambientCoefficient;\n"
                            "   vec3 ambientColor;\n"
                            "   vec4 diffuseColor; // a.k.a. surface color\n"
                            "} material;"
                            "\n"
                            "uniform struct Light {\n"
                            "   vec3 color;\n"
                            "   vec3 position;\n"
                            "   vec3 direction;\n"
                            "   float attenuation;\n"
                            "   float cutoffCos;\n"
                            "   float spotExponent;\n"
                            "} light;\n"
                            "\n";

static const char* phong_func =
                            "vec4 getColor(vec3 fragNormal, vec3 fragVert, mat4 model, mat3 normalMatrix, vec3 cameraPosition, Material material, Light light) {"
                            "    vec3 normal = normalize(normalMatrix * fragNormal);\n"
                            "    vec3 surfacePos = vec3(model * vec4(fragVert, 1));\n"
                            "    vec3 surfaceToLight = normalize(light.position - surfacePos);\n"
                            "    vec3 surfaceToCamera = normalize(cameraPosition - surfacePos);\n"
                            "    \n"
                            "    //ambient\n"
                            "    vec3 ambient = material.ambientCoefficient * material.ambientColor.rgb * light.color;\n"
                            "\n"
                            "    //diffuse\n"
                            "    float diffuseCoefficient = max(0.0, dot(normal, surfaceToLight));\n"
                            "    vec3 diffuse = diffuseCoefficient * material.diffuseColor.rgb * light.color;\n"
                            "    \n"
                            "    //specular\n"
                            "    float specularCoefficient = 0.0;\n"
                            "    if(diffuseCoefficient > 0.0)\n"
                            "        specularCoefficient = pow(max(0.0, dot(surfaceToCamera, reflect(-surfaceToLight, normal))), material.shininess);\n"
                            "    vec3 specular = specularCoefficient * material.specularColor * light.color;\n"
                            "    \n"
                            "    //attenuation\n"
                            "    float distanceToLight = length(light.position - surfacePos);\n"
                            "    float distAttenuation = 1.0 / (1.0 + light.attenuation * pow(distanceToLight, 2));\n"
                            "\n"
                            "    float spotFactor = dot(-surfaceToLight, normalize(light.direction));\n"
                            "    float angularAttenuation = 0.0;\n"
                            "    if (spotFactor > light.cutoffCos) {\n"
                            "         angularAttenuation = 1.0 - (1.0 - spotFactor) * 1.0/ (1.0 - light.cutoffCos);\n"
                            "         angularAttenuation = pow(angularAttenuation, light.spotExponent);"
                            "    }\n"
                            "    float attenuation = distAttenuation * angularAttenuation;\n"
                            "    return vec4(ambient + attenuation*(diffuse + specular), material.diffuseColor.a);\n"
                            "}\n";

static const char* blin_phong_func =
                            "vec4 getColor(vec3 fragNormal, vec3 fragVert, mat4 model, mat3 normalMatrix, vec3 cameraPosition, Material material, Light light) {"
                            "    vec3 normal = normalize(normalMatrix * fragNormal);\n"
                            "    vec3 surfacePos = vec3(model * vec4(fragVert, 1));\n"
                            "    vec3 surfaceToLight = normalize(light.position - surfacePos);\n"
                            "    vec3 surfaceToCamera = normalize(cameraPosition - surfacePos);\n"
                            "    \n"
                            "    //ambient\n"
                            "    vec3 ambient = material.ambientCoefficient * material.ambientColor.rgb * light.color;\n"
                            "\n"
                            "    //diffuse\n"
                            "    float diffuseCoefficient = max(0.0, dot(normal, surfaceToLight));\n"
                            "    vec3 diffuse = diffuseCoefficient * material.diffuseColor.rgb * light.color;\n"
                            "    \n"
                            "    //specular\n"
                            "    float specularCoefficient = 0.0;\n"
                            "    if(diffuseCoefficient > 0.0) {\n"
                            "        vec3 half_way = normalize(surfaceToLight + surfaceToCamera); // better way is to calculate it in vertex shader\n"
                            "        specularCoefficient = pow(max(0.0, dot(normal, half_way)), material.shininess);\n"
                            "    }\n"
                            "    vec3 specular = specularCoefficient * material.specularColor * light.color;\n"
                            "    \n"
                            "    //attenuation\n"
                            "    float distanceToLight = length(light.position - surfacePos);\n"
                            "    float distAttenuation = 1.0 / (1.0 + light.attenuation * pow(distanceToLight, 2));\n"
                            "\n"
                            "    float spotFactor = dot(-surfaceToLight, normalize(light.direction));\n"
                            "    float angularAttenuation = 0.0;\n"
                            "    if (spotFactor > light.cutoffCos) {\n"
                            "         angularAttenuation = 1.0 - (1.0 - spotFactor) * 1.0/ (1.0 - light.cutoffCos);\n"
                            "         angularAttenuation = pow(angularAttenuation, light.spotExponent);"
                            "    }\n"
                            "    float attenuation = distAttenuation * angularAttenuation;\n"
                            "    return vec4(ambient + attenuation*(diffuse + specular), material.diffuseColor.a);\n"
                            "}\n";

static const char* frg_sh_per_frag =
                            "void main() {\n"
                            "    gl_FragColor = getColor(fragNormal, fragVert, model, normalMatrix, cameraPosition, material, light);\n"
                            "}";

////////////////////////////////////////////////////////////////////////////////

static const char* vtx_sh_header_per_vtx = "#version 120\n"
                            "uniform mat4 camera;\n"
                            "uniform mat4 model;\n"
                            "uniform mat3 normalMatrix;\n"
                            "uniform vec3 cameraPosition;\n"
                            "\n"
                            "uniform struct Material {\n"
                            "   float shininess;\n"
                            "   vec3 specularColor;\n"
                            "   float ambientCoefficient;\n"
                            "   vec3 ambientColor;\n"
                            "   vec4 diffuseColor; // a.k.a. surface color\n"
                            "} material;"
                            "\n"
                            "uniform struct Light {\n"
                            "   vec3 color;\n"
                            "   vec3 position;\n"
                            "   vec3 direction;\n"
                            "   float attenuation;\n"
                            "   float cutoffCos;\n"
                            "   float spotExponent;\n"
                            "} light;\n"
                            "\n"
                            "attribute vec3 vert;\n"
                            "attribute vec3 vertNormal;\n"
                            "varying vec4 fragColor;\n"
                            "\n";
static const char* vtx_sh_per_vtx =
                            "void main() {\n"
                            "    fragColor = getColor(vertNormal, vert, model, normalMatrix, cameraPosition, material, light);\n"
                            "    gl_Position = camera * model * vec4(vert, 1);\n"
                            "}";

static const char* frg_sh_per_vtx = "#version 120\n"
                            "varying vec4 fragColor;\n"
                            "void main() {\n"
                            "    gl_FragColor = fragColor;\n"
                            "}";
////////////////////////////////////////////////////////////////////////////////

static const char* vtx_sh_header_per_facet = "#version 120\n"
                            "#extension GL_EXT_gpu_shader4 : enable\n"
                            "uniform mat4 camera;\n"
                            "uniform mat4 model;\n"
                            "uniform mat3 normalMatrix;\n"
                            "uniform vec3 cameraPosition;\n"
                            "\n"
                            "uniform struct Material {\n"
                            "   float shininess;\n"
                            "   vec3 specularColor;\n"
                            "   float ambientCoefficient;\n"
                            "   vec3 ambientColor;\n"
                            "   vec4 diffuseColor; // a.k.a. surface color\n"
                            "} material;"
                            "\n"
                            "uniform struct Light {\n"
                            "   vec3 color;\n"
                            "   vec3 position;\n"
                            "   vec3 direction;\n"
                            "   float attenuation;\n"
                            "   float cutoffCos;\n"
                            "   float spotExponent;\n"
                            "} light;\n"
                            "\n"
                            "attribute vec3 vert;\n"
                            "attribute vec3 vertNormal;\n"
                            "flat varying vec4 fragColor;\n"
                            "\n";

static const char* vtx_sh_per_facet =
                            "void main() {\n"
                            "    fragColor = getColor(vertNormal, vert, model, normalMatrix, cameraPosition, material, light);\n"
                            "    gl_Position = camera * model * vec4(vert, 1);\n"
                            "}";

static const char* frg_sh_per_facet = "#version 120\n"
                            "#extension GL_EXT_gpu_shader4 : enable\n"
                            "flat varying vec4 fragColor;\n"
                            "void main() {\n"
                            "    gl_FragColor = fragColor;\n"
                            "}";

enum TYPE_FUNC { TF_PHONG = 0, TF_BLIN_PHONG };
enum TYPE_SHADING {TS_PER_FCT = 0, TS_PER_VTX, TS_PER_FRG};

inline std::pair<std::string, std::string> buildShaders(TYPE_FUNC tFunc, TYPE_SHADING tShading) {
  using namespace std;
  string func = tFunc == TF_PHONG ? phong_func : blin_phong_func;

  string vtx_sh;
  string frg_sh;

  switch (tShading) {
    case TS_PER_FRG:
      vtx_sh = vtx_sh_per_frag;
      frg_sh = frg_sh_header_per_frag + func + frg_sh_per_frag;
      break;
    case TS_PER_VTX:
      vtx_sh = vtx_sh_header_per_vtx + func + vtx_sh_per_vtx;
      frg_sh = frg_sh_per_vtx;
      break;
    case TS_PER_FCT:
      vtx_sh = vtx_sh_header_per_facet + func + vtx_sh_per_facet;
      frg_sh = frg_sh_per_facet;
      break;
    default:
      assert(false);
  }
  return make_pair(vtx_sh, frg_sh);
}
////////////////////////////////////////////////////////////////////////////////

static const char* vtx_sh_cone = "#version 120\n"
                              "\n"
                              "uniform mat4 camera;\n"
                              "uniform mat4 model;\n"
                              "\n"
                              "attribute vec3 vert;\n"
                              "\n"
                              "void main() {\n"
                              "    \n"
                              "    gl_Position = camera * model * vec4(vert, 1);\n"
                              "}";

static const char* frg_sh_cone = "#version 120\n"
                              "void main() {\n"
                              "    gl_FragColor = vec4(0.5, 0.5, 0, 0.5);\n"
                              "}";
#endif
