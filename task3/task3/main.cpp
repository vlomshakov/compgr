//
//  main.cpp
//  task2
//
//  Created by Vadim Lomshakov on 06/12/13.
//  Copyright (c) 2013 spbau. All rights reserved.
//

#include <iostream>
#include <GL/glew.h>
#include <GL/freeglut.h>

#include <vector>
#include <cmath>

#include <AntTweakBar.h>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include "ModelInstance.h"
#include "Camera.h"
#include "BunnyModel.h"
#include "PlaneModel.h"
#include "ConesModel.h"


static void onResize(int width, int height);
static void render();

// globals
std::vector<std::shared_ptr<ModelInstance>> gInstances;

Camera gCamera;
LightSourceData gLightSrc(glm::vec3(0.0f, 17.0f, 0.0f), glm::vec3(1, 1, 1), 0.001f, (float) cos(M_PI/8.0), glm::vec3(0.0f, -1.0f, 0.0f), 1);
MaterialData gMaterial = {
    .shininess = 20.0f,
    .specularColor = glm::vec3(1, 1, 1),
    .ambientCoefficient = 0.01f,
    .ambientColor = glm::vec3(0.5, 0.0, 0.0),
    .diffuseColor = glm::vec4(0.0, 0.7, 0.0, 1.0)
};

static void initCallbacks() {
  glutReshapeFunc(onResize);
  glutDisplayFunc(render);
}

static void onResize(int width, int height) {
  gCamera.setViewportAspectRatio((float)width / height);
  glViewport(0, 0, width, height);
  TwWindowSize(width, height);
  glutPostRedisplay();
}

void mouseButtonFun(int glutButton, int glutState, int mouseX, int mouseY) {
  TwEventMouseButtonGLUT(glutButton, glutState, mouseX, mouseY);
  glutPostRedisplay();
}
void mouseMotionFun(int mouseX, int mouseY) {
  TwEventMouseMotionGLUT(mouseX, mouseY);
  glutPostRedisplay();
}
void keyboardFun(unsigned char glutKey, int mouseX, int mouseY) {
  TwEventKeyboardGLUT(glutKey, mouseX, mouseY);
  glutPostRedisplay();
}
void specialFun(int glutKey, int mouseX, int mouseY) {
  TwEventSpecialGLUT(glutKey, mouseX, mouseY);
  glutPostRedisplay();
}

void setupGUI() {
  // Initialize AntTweakBar
  TwInit(TW_OPENGL, NULL);

  glutMouseFunc(mouseButtonFun);
  glutMotionFunc(mouseMotionFun);
  glutPassiveMotionFunc(mouseMotionFun);
  glutKeyboardFunc(keyboardFun);
  glutSpecialFunc(specialFun);
  TwGLUTModifiersFunc(glutGetModifiers);

  // Create a tweak bar

  TwBar *bar = TwNewBar("Tweaks");
  TwDefine(" Tweaks size='240 570' ");

  TwAddVarCB(bar, "FOV", TW_TYPE_FLOAT, Camera::setFovCallback, Camera::getFovCallback, &gCamera, "min=0.01 max=100 step=1 keyIncr=s keyDecr=w group='View Setup'");
  TwAddVarCB(bar, "Near", TW_TYPE_FLOAT, Camera::setNearCallback, Camera::getNearCallback, &gCamera, "min=0.01 max=100 step=.5 keyIncr=u keyDecr=U group='View Setup'");
  TwAddVarCB(bar, "Far", TW_TYPE_FLOAT, Camera::setFarCallback, Camera::getFarCallback, &gCamera, "min=0.01 max=100 step=.5 keyIncr=e keyDecr=E group='View Setup'");

  TwAddVarCB(bar, "Cam Rotation", TW_TYPE_QUAT4F, Camera::setCamOrientCallback, Camera::getCamOrientCallback, &gCamera, "opened=true group='View Setup'");


  TwAddVarRW(bar, "Color", TW_TYPE_COLOR3F, glm::value_ptr(gLightSrc.color), "group='Light Setup'");

  TwAddVarRW(bar, "Pos X", TW_TYPE_FLOAT, &gLightSrc.position.x, "group='Light Position' ");
  TwAddVarRW(bar, "Pos Y", TW_TYPE_FLOAT, &gLightSrc.position.y, "group='Light Position' ");
  TwAddVarRW(bar, "Pos Z", TW_TYPE_FLOAT, &gLightSrc.position.z, "group='Light Position' ");
  TwAddVarRW(bar, "Attenuation Coeff", TW_TYPE_FLOAT, &gLightSrc.attenuation, "min=0.001 max=100 step=.001 group='Light Setup' ");
  TwAddVarRW(bar, "Spot exponent", TW_TYPE_FLOAT, &gLightSrc.spotExponent, "min=0.01 max=100 step=.01 group='Light Setup' ");
  TwAddVarRW(bar, "Cutoff cosin", TW_TYPE_FLOAT, &gLightSrc.cutoffCos, "min=0.00 max=1 step=.01 group='Light Setup' ");
  TwAddVarRW(bar, "Light dir", TW_TYPE_DIR3F, (void* )glm::value_ptr(gLightSrc.direction), "opened=true group='Light Setup' ");


  TwAddVarRW(bar, "Ambient color", TW_TYPE_COLOR3F, (void*)glm::value_ptr(gMaterial.ambientColor), "group='Material Setup'");
  TwAddVarRW(bar, "Ambient coefficient", TW_TYPE_FLOAT, &gMaterial.ambientCoefficient, "min=0.001 max=100 step=.001 group='Material Setup'");
  TwAddVarRW(bar, "Diffuse color", TW_TYPE_COLOR4F, (void*)glm::value_ptr(gMaterial.diffuseColor), "group='Material Setup'");
  TwAddVarRW(bar, "Specular color", TW_TYPE_COLOR3F, (void*)glm::value_ptr(gMaterial.specularColor), "group='Material Setup'");
  TwAddVarRW(bar, "Specular shininess", TW_TYPE_FLOAT, &gMaterial.shininess, "min=0.0 max=100.0 step=1.0  group='Material Setup' ");
}

void handlePhSelection(int m) {
  for (size_t i = 0; i != gInstances.size(); ++i)
    gInstances.at(i)->setShadingAndFuncType((TYPE_SHADING)m, TF_PHONG);
}
void handleBPhSelection(int m) {
  for (size_t i = 0; i != gInstances.size(); ++i)
    gInstances.at(i)->setShadingAndFuncType((TYPE_SHADING)m, TF_BLIN_PHONG);
}

void createMenu() {
  int phongSubmenu = glutCreateMenu(handlePhSelection);
  glutAddMenuEntry("Flat", 0);
  glutAddMenuEntry("Vtx", 1);
  glutAddMenuEntry("Frag", 2);

  int blinPhongSubmenu = glutCreateMenu(handleBPhSelection);
  glutAddMenuEntry("Flat", 0);
  glutAddMenuEntry("Vtx", 1);
  glutAddMenuEntry("Frag", 2);

  glutCreateMenu(0);
  glutAddSubMenu("Phong", phongSubmenu);
  glutAddSubMenu("Blinn-Phong", blinPhongSubmenu);

  glutAttachMenu(GLUT_RIGHT_BUTTON);
}

static void createInstances() {
  std::shared_ptr<ModelInstance> bunny((ModelInstance*)new BunnyModel(glm::scale(glm::translate(glm::mat4(), glm::vec3(1, -1.5f, -2)), glm::vec3(5, 5, 5))));
  gInstances.push_back(bunny);

  std::shared_ptr<ModelInstance> plane((ModelInstance*)new PlaneModel(
      glm::scale(glm::mat4(), glm::vec3(10, 10, 10))));
  gInstances.push_back(plane);

  std::shared_ptr<ModelInstance> cone((ModelInstance*)new ConesModel(glm::mat4(), gLightSrc));
  gInstances.push_back(cone);
}

static void render() {
  glClearColor(0, 0, 0, 1);
  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);


  for (size_t i = 0; i != gInstances.size(); ++i)
    gInstances.at(i)->draw(gCamera, gLightSrc, gMaterial);


  TwDraw();
  glutSwapBuffers();
}


int main(int argc, const char * argv[])
{
  glutInit(&argc, (char **) argv);
  glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGBA | GLUT_DEPTH);
  glutInitWindowSize(800, 600);
  glutInitWindowPosition(100, 100);
  glutCreateWindow("task 3");

  GLenum res = glewInit();
  if (res != GLEW_OK)
  {
    std::cerr <<  "Error: " <<  glewGetErrorString(res) << std::endl;
    exit(1);
  }

  try {
    createInstances();
    createMenu();
    initCallbacks();
    setupGUI();
  } catch (std::exception &e) {
    std::cerr << e.what() << std::endl;
  }

  glClearColor(0, 0, 0, 1);

  // init common state
  glEnable(GL_DEPTH_TEST);
  glDepthFunc(GL_LESS);

  glutMainLoop();
  return 0;
}
