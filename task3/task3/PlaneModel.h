//
// Created by Vadim Lomshakov on 24/12/13.
// Copyright (c) 2013 spbau. All rights reserved.
//


#ifndef __PlaneModel_H_
#define __PlaneModel_H_

#include <string>
#include <cmath>
#include <vector>
#include "ModelAsset.h"
#include "Shaders.h"
#include "ObjFileLoader.h"
#include "LightSourceData.h"
#include "ModelInstance.h"
#include "Camera.h"
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>


class PlaneModel : ModelInstance {
  TYPE_SHADING typeShading;
  TYPE_FUNC typeFunc;
public:

  PlaneModel() :
  typeShading(TS_PER_FRG),
  typeFunc(TF_PHONG)
  {
    loadPlaneAsset();
  }

  PlaneModel(glm::mat4 transformMat) :
  ModelInstance(transformMat),
  typeShading(TS_PER_FRG),
  typeFunc(TF_PHONG)
  {
    loadPlaneAsset();
  }

  void draw(Camera const &camera, LightSourceData const &lightSrcData, MaterialData const &material) {
    glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
    glm::mat4 model = camera.modelQuat() * transformModel;
    glm::mat3 normalMatrix = glm::transpose(glm::inverse(glm::mat3(model)));
    setNewMaterial(material);
//    asset.material.diffuseColor = glm::vec4(0.5,0.5,0.5,1);
    asset.shaders->use();

    glBindBuffer(GL_ARRAY_BUFFER, asset.vbo);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, asset.ibo);

    glEnableVertexAttribArray(asset.shaders->attLc("vert"));
    glEnableVertexAttribArray(asset.shaders->attLc("vertNormal"));

    GLsizei stride = 6 * sizeof(GLfloat);
    glVertexAttribPointer(asset.shaders->attLc("vert"), 3, GL_FLOAT, GL_FALSE, stride, NULL);
    glVertexAttribPointer(asset.shaders->attLc("vertNormal"), 3, GL_FLOAT, GL_TRUE, stride, (const GLvoid *)(3*sizeof(GLfloat)));


    glUniform3fv(asset.shaders->uniLc("light.color"), 1, glm::value_ptr(lightSrcData.color));
    glUniform3fv(asset.shaders->uniLc("light.position"), 1, glm::value_ptr(lightSrcData.position));
    glUniform3fv(asset.shaders->uniLc("light.direction"), 1, glm::value_ptr(lightSrcData.direction));
    glUniform1f(asset.shaders->uniLc("light.attenuation"), lightSrcData.attenuation);
    glUniform1f(asset.shaders->uniLc("light.cutoffCos"), lightSrcData.cutoffCos);
    glUniform1f(asset.shaders->uniLc("light.spotExponent"), lightSrcData.spotExponent);

    glUniform1f(asset.shaders->uniLc("material.shininess"), asset.material.shininess);
    glUniform3fv(asset.shaders->uniLc("material.specularColor"),1, glm::value_ptr(asset.material.specularColor));
    glUniform1f(asset.shaders->uniLc("material.ambientCoefficient"), asset.material.ambientCoefficient);
    glUniform3fv(asset.shaders->uniLc("material.ambientColor"),1, glm::value_ptr(asset.material.ambientColor));
    glUniform4fv(asset.shaders->uniLc("material.diffuseColor"),1, glm::value_ptr(asset.material.diffuseColor));

    glUniform3fv(asset.shaders->uniLc("cameraPosition"),1, glm::value_ptr(camera.position()));
    glUniformMatrix4fv(asset.shaders->uniLc("camera"), 1.0f, GL_FALSE, glm::value_ptr(camera.projectionView()));
    glUniformMatrix4fv(asset.shaders->uniLc("model"), 1.0f, GL_FALSE, glm::value_ptr(model));
    glUniformMatrix3fv(asset.shaders->uniLc("normalMatrix"), 1.0f, GL_FALSE, glm::value_ptr(normalMatrix));
    glDrawElements(asset.drawType, asset.drawCount, GL_UNSIGNED_INT, 0);


    glDisableVertexAttribArray(asset.shaders->attLc("vert"));
    glDisableVertexAttribArray(asset.shaders->attLc("vertNormal"));

    glBindBuffer(GL_ARRAY_BUFFER, 0);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);

    asset.shaders->stopUsing();
  }

  void setShadingPerFrg() { typeShading = TS_PER_FRG; updateShaders(); }

  void setShadingPerVtx() { typeShading = TS_PER_VTX; updateShaders(); }

  void setShadingPerFct() { typeShading = TS_PER_FCT; updateShaders(); }

  void setPhongFunc() { typeFunc = TF_PHONG; updateShaders(); }

  void setBlinPhongFunc() { typeFunc = TF_BLIN_PHONG; updateShaders(); }

  void setShadingAndFuncType(TYPE_SHADING shadingType, TYPE_FUNC funcType) {
    typeShading = shadingType;
    typeFunc = funcType;
    updateShaders();
  }

  ~PlaneModel() {}
private:
  void updateShaders() {
    std::pair<std::string, std::string>  shaders = buildShaders(typeFunc, typeShading);
    asset.shaders.reset(new Program(shaders.first.c_str(), shaders.second.c_str()));
  }

  void loadPlaneAsset() {
    std::pair<std::string, std::string>  shaders = buildShaders(typeFunc, typeShading);
    asset.shaders.reset(new Program(shaders.first.c_str(), shaders.second.c_str()));
    asset.drawType = GL_TRIANGLES;
    asset.material.shininess = 20.0f;
    asset.material.specularColor = glm::vec3(1, 1, 1);
    asset.material.ambientCoefficient = 0.005f;
    asset.material.ambientColor = glm::vec3(0.f, 0.3f, 0.0f);
    asset.material.diffuseColor = glm::vec4(0.f, 0.3f, 0.0f, 1.0f);

    asset.drawCount =  2*3;

    // orientation is CCW
    GLfloat vertexData[] = {
        //X    Y    Z            NX  NY  NZ
        1.0f, 0, -1.0f,          0,  1,  0,
        1.0f, 0,  1.0f,          0,  1,  0,
        -1.0f,0,  1.0f,          0,  1,  0,
        -1.0f,0, -1.0f,          0,  1,  0
    };

    GLint idxData[] = {
        2, 1 , 0,
        2, 0 , 3
    };

    glGenBuffers(1, &asset.vbo);
    glBindBuffer(GL_ARRAY_BUFFER, asset.vbo);
    glBufferData(GL_ARRAY_BUFFER, sizeof(vertexData), vertexData, GL_STATIC_DRAW);

    glGenBuffers(1, &asset.ibo);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, asset.ibo);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(idxData),
        idxData, GL_STATIC_DRAW);

    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
  }
};


#endif //__PlaneModel_H_
