deps: glew, freeglut, opengl3.2/glsl1.2 , glm

for run:
        
        $cd ./task1
        $make run


keys:
    w/s - decrease/increase fov
    e/d - decrease/increase zFar
    r/f -  decrease/increase zNear

