//
//  main.cpp
//  task1
//
//  Created by Vadim Lomshakov on 10/9/13.
//  Copyright (c) 2013 spbau. All rights reserved.
//


#include "MainWindow.h"

int main(int argc, const char * argv[])
{
  graphics::MainWindow::getInstance().runEventLoop();

  return EXIT_SUCCESS;
}
