#include "ObjLoader.h"

#include <fstream>

namespace graphics {
  char const * const ObjLoader::VERT_PREFIX = "v";
  char const * const ObjLoader::UV_PREFIX = "vt";
  char const * const ObjLoader::NORMAL_PREFIX = "vn";
  char const * const ObjLoader::FACE_PREFIX = "f";

  void ObjLoader::loadObjFile(char const *path, std::vector<glm::vec3>& outVer, std::vector<unsigned int>& outVerIndices) {
    using namespace std;
    ifstream  in;

    in.open(path);

    string line;
    string prefix;
    while (in) {
      in >> prefix ;

      if (prefix == VERT_PREFIX) {
        glm::vec3 vertex;
        in >> vertex.x >> vertex.y >> vertex.z;
        outVer.push_back(vertex);

      } else if (prefix == UV_PREFIX) {
        //skip
        getline(in, line);
        line.clear();

      } else if (prefix == NORMAL_PREFIX) {
        //skip
        getline(in, line);
        line.clear();

      } else if (prefix == FACE_PREFIX) {
        unsigned int vertexIndex[3], uvIndex[3], normalIndex[3];
        char sep;
        in >> vertexIndex[0] >> sep >> uvIndex[0] >> sep >>  normalIndex[0] >>
              vertexIndex[1] >> sep >> uvIndex[1] >> sep >> normalIndex[1] >>
              vertexIndex[2] >> sep >> uvIndex[2] >> sep >> normalIndex[2];

        outVerIndices.push_back(vertexIndex[0] - 1);
        outVerIndices.push_back(vertexIndex[1] - 1);
        outVerIndices.push_back(vertexIndex[2] - 1);

      } else if (!prefix.empty()) {
        // skip other
        getline(in, line);
        line.clear();
      }
    }

    return;
  }

}