//
// Created by Vadim Lomshakov on 10/15/13.
// Copyright (c) 2013 lomshakov.
//
//

#include "MainWindow.h"
#include <fstream>


namespace graphics {
  #define MENU_FST_WAY 1
  #define MENU_SND_WAY 2
  #define MENU_MESH 3

  MainWindow* MainWindow::_instance = 0;

  MainWindow::MainWindow(int argc, char **argv)
    : _width(512.0f), _height(512.0f) ,
      _posX(100), _posY(100),
      _title("task1"),
      _fov(30.0f), _heading(0.0f), _pitch(0.0f), _zNear(5.0f), _zFar(100.0f),
      typeColoring(1)
  {
    // passes outer params
    glutInit(&argc, argv);
  }

  void MainWindow::runEventLoop() {
    try {
      initGlState();
      glutMainLoop();
    } catch (std::ifstream::failure const & e) {
      std::cerr << "Explanatory string: " << e.what() << std::endl;
    } catch(std::exception & e) {
      std::cerr << e.what() << std::endl;
    }
  }

  void MainWindow::initGlState() {
    glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGBA | GLUT_DEPTH);
    glutInitWindowSize((int)_width, (int)_height);
    glutInitWindowPosition(_posX, _posY);
    glutCreateWindow(_title.c_str());

    glutCreateMenu(wrapperOnPopupMenu);
    glutAddMenuEntry("First way", MENU_FST_WAY);
    glutAddMenuEntry("Second way", MENU_SND_WAY);
    glutAddMenuEntry("Mesh", MENU_MESH);
    glutAttachMenu(GLUT_RIGHT_BUTTON);


    initGlutCallbacks();

    GLenum res = glewInit();
    if (res != GLEW_OK)
    {
      std::cerr <<  "Error: " <<  glewGetErrorString(res) << std::endl;
      exit(1);
    }

    glClearColor(.15f, .15f, .15f, 1.0f);

    // Enable depth test
    glEnable(GL_DEPTH_TEST);
    glDepthFunc(GL_LEQUAL);

    std::vector<glm::vec3> outVer;
    std::vector<unsigned int> outVerInd;

#ifndef _NO_IDE_
    ObjLoader::loadObjFile("/Users/vlomshakov/Documents/Vadik/graphics/tasks/task1/task1/model.obj", outVer, outVerInd);
#else
    ObjLoader::loadObjFile("./model.obj", outVer, outVerInd);
#endif
    createVertexBuffer(outVer);
    createIndexBuffer(outVerInd);

    compileShaders();
  }

  void MainWindow::initGlutCallbacks()
  {
    glutDisplayFunc(wrapperOnRenderScene);
    glutReshapeFunc(wrapperOnReshapeWindow);
    glutKeyboardFunc(wrapperOnKeyboard);
    glutMouseFunc(wrapperOnMouse);
  }

  void MainWindow::compileShaders() {
#ifndef _NO_IDE_
    ShaderInfo  shadersModel[] = {
        { GL_VERTEX_SHADER, "/Users/vlomshakov/Documents/Vadik/graphics/tasks/task1/task1/model.v.glsl" },
        { GL_FRAGMENT_SHADER, "/Users/vlomshakov/Documents/Vadik/graphics/tasks/task1/task1/model.f.glsl" },
        { GL_NONE, NULL }
    };

    ShaderInfo  shadersMesh[] = {
        { GL_VERTEX_SHADER, "/Users/vlomshakov/Documents/Vadik/graphics/tasks/task1/task1/mesh.v.glsl" },
        { GL_FRAGMENT_SHADER, "/Users/vlomshakov/Documents/Vadik/graphics/tasks/task1/task1/mesh.f.glsl" },
        { GL_NONE, NULL }
    };
#else
     ShaderInfo  shadersModel[] = {
        { GL_VERTEX_SHADER, "./model.v.glsl" },
        { GL_FRAGMENT_SHADER, "./model.f.glsl" },
        { GL_NONE, NULL }
    };

    ShaderInfo  shadersMesh[] = {
        { GL_VERTEX_SHADER, "./mesh.v.glsl" },
        { GL_FRAGMENT_SHADER, "./mesh.f.glsl" },
        { GL_NONE, NULL }
    };
#endif

    programModel = LoadShaders(shadersModel);
    programMesh = LoadShaders(shadersMesh);
    gModelMVP = glGetUniformLocation(programModel, "gMVP");
    assert(gModelMVP != -1);
    gNear = glGetUniformLocation(programModel, "near");
    gFar = glGetUniformLocation(programModel, "far");

    gTypeColoring = glGetUniformLocation(programModel, "typeColoring");
    assert(gTypeColoring != -1);
    gMeshMVP = glGetUniformLocation(programMesh, "gMVP");
    assert(gMeshMVP != -1);

  }

  void MainWindow::createIndexBuffer(std::vector<unsigned int> const & indices) {
    _sizeIndices = (int)indices.size();
    glGenBuffers(1, &IBO);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, IBO);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(unsigned int) * indices.size(), &indices[0], GL_STATIC_DRAW);
  }

  void MainWindow::createVertexBuffer(std::vector<glm::vec3> const & vertices) {
    glGenBuffers(1, &VBO);
    glBindBuffer(GL_ARRAY_BUFFER, VBO);
    glBufferData(GL_ARRAY_BUFFER, sizeof(glm::vec3) * vertices.size() , &vertices[0], GL_STATIC_DRAW);
  }


  void MainWindow::onRenderScene() {
    glm::mat4 modelMat = glm::rotate(glm::mat4(1.0f), -90.0f, glm::vec3(0, 1, 0));
    glm::mat4 mvpMat = getViewProjectionMat() * modelMat;

    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    // set settings
    glEnableVertexAttribArray(0);

    glBindBuffer(GL_ARRAY_BUFFER, VBO);
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, 0);

    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, IBO);


    // draw model
    glUseProgram(programModel);
    glUniformMatrix4fv(gModelMVP, 1, GL_FALSE, glm::value_ptr(mvpMat)); // false because openGl use column-major for storing matrix by default
    glUniform1f(gNear, _zNear);
    glUniform1f(gFar, _zFar);
    glUniform1i(gTypeColoring, typeColoring);
    glDrawElements(GL_TRIANGLES, _sizeIndices, GL_UNSIGNED_INT, 0);

    if (onMesh) {
      //draw mesh
      glEnable(GL_POLYGON_OFFSET_LINE);
      glPolygonOffset(-0.5,-0.5);
      glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);

      glUseProgram(programMesh);
      glUniformMatrix4fv(gMeshMVP, 1, GL_FALSE, glm::value_ptr(mvpMat));
      glDrawElements(GL_TRIANGLES, _sizeIndices, GL_UNSIGNED_INT, 0);

      // restore default polygon mode
      glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
      glDisable(GL_POLYGON_OFFSET_LINE);
    }


    // reset setting
    glDisableVertexAttribArray(0);

    glutSwapBuffers();
  }

  glm::mat4 MainWindow::getViewProjectionMat() {

    glm::mat4 projectionMat = glm::perspective(_fov, _width / _height , _zNear, _zFar);

    glm::mat4 viewMat = glm::lookAt(glm::vec3(0, 0, 30), glm::vec3(0, 0, 0), glm::vec3(0, 1, 0));

    glm::mat4 rotationYViewMat = glm::rotate(glm::mat4(1.0f), -_heading, glm::vec3(0, 1, 0));

    glm::mat4 rotationXViewMat = glm::rotate(glm::mat4(1.0f), -_pitch, glm::vec3(1, 0, 0));

    return projectionMat * viewMat * rotationYViewMat * rotationXViewMat;
  }

  void MainWindow::wrapperOnRenderScene() {
    MainWindow::_instance->onRenderScene();
  }

//-------------------------------------------//
//-      Other                              -//
//-------------------------------------------//

  void MainWindow::setWindowPosition(glm::ivec2 position) {
    _posX = position.x;
    _posY = position.y;
  }

  void MainWindow::setWindowSize(int width, int height) {
    _width = width;
    _height = height;
  }

  void MainWindow::setWindowTitle(std::string title) {
    _title = title;
  }

  MainWindow& MainWindow::getInstance(int argc, char **argv) {
    static MainWindow window(argc, argv);
    if (MainWindow::_instance == 0)
      MainWindow::_instance = &window;
    return window;
  }


//-------------------------------------------//
//-      Handlers & Wrappers                -//
//-------------------------------------------//

  void MainWindow::onPopupMenu(int option) {
    switch (option) {
      case MENU_FST_WAY:
        typeColoring = 1;
        break;
      case MENU_SND_WAY:
        typeColoring = 2;
        break;
      case MENU_MESH:
        onMesh = !onMesh;
        break;
    }
    glutPostRedisplay();
  }

  void MainWindow::onReshapeWindow(int width, int height) {
    _width = width;
    _height = height;
    glViewport(0, 0, _width, _height);
    glutPostRedisplay();
  }

  void MainWindow::onKeyboard(unsigned char key, int i, int i1) {
    switch (key) {
      // control fov
      case 'w':
      case 'W':
        _fov -= 1.0f;
        break;
      case 's':
      case 'S':
        _fov += 1.0f;
        break;

      // control zFar
      case 'd':
        _zFar += 1.0f;
        break;
      case 'e':
        _zFar -= 1.0f;
        break;

      // control zNear
      case 'f':
        _zNear += 1.0f;
        break;
      case 'r':
        _zNear -= 1.0f;
        break;

      case 'x':
        exit(EXIT_SUCCESS);
    }
    glutPostRedisplay();
  }

  void MainWindow::onMoveMouse(int x, int y) {
    static int _prevX = 0;
    static int _prevY = 0;
    int diffX = x - _prevX;
    int diffY = y - _prevY;

    if (diffX > 0)
      _heading += 1;
    if (diffX < 0)
      _heading -= 1;
    _prevX = x;

    if (diffY > 0)
      _pitch += 1;
    if (diffY < 0)
      _pitch -= 1;
    _prevY = y;

    glutPostRedisplay();
  }

  void MainWindow::onMouse(int button, int state, int x, int y) {
    if (button == GLUT_LEFT_BUTTON) {
      if (state == GLUT_DOWN) {
        glutMotionFunc(wrapperOnMoveMouse);
      } else {
        glutMotionFunc(NULL);
      }
    }
  }

  void MainWindow::wrapperOnReshapeWindow(int width, int height) {
    MainWindow::_instance->onReshapeWindow(width, height);
  }

  void MainWindow::wrapperOnMoveMouse(int x, int _y) {
    MainWindow::_instance->onMoveMouse(x, _y);
  }

  void MainWindow::wrapperOnKeyboard(unsigned char key, int i, int i1) {
    MainWindow::_instance->onKeyboard(key, i, i1);
  }

  void MainWindow::wrapperOnMouse(int button, int state, int x, int y) {
    MainWindow::_instance->onMouse(button, state, x, y);
  }

  void MainWindow::wrapperOnPopupMenu(int option) {
    MainWindow::_instance->onPopupMenu(option);
  }
}
