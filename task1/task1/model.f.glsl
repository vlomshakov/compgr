#version 120



 uniform int typeColoring;

 varying float zVar;

 void main()
 {
   if (typeColoring == 1) {
     gl_FragColor = vec4(zVar, zVar, zVar, 1.0f );
   } else {
     const float n = 5.0f;
     const float f = 100.0f;

     float zClipperSpace = mix(-1.0f, 1.0f, gl_FragCoord.z);
     float zEyeSpace = (- 2 * n * f) / (f + n - zClipperSpace * (f - n));
     float zVar2 = (-zEyeSpace - n) / (f - n);

     gl_FragColor = vec4(zVar2,zVar2,zVar2,1.0f);
   }
 }