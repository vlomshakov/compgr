//
// Created by Vadim Lomshakov on 10/15/13.
// Copyright (c) 2013 lomshakov.
//
//



#ifndef __MainWindow_H_
#define __MainWindow_H_

#include <iostream>
#include <cassert>
#include <string>
#include "vgl.h"
#include "LoadShaders.h"
#include "ObjLoader.h"
#include <stdexcept>


namespace graphics {
  class MainWindow {
  public:

    static MainWindow& getInstance(int argc = 0, char ** argv = 0);

    void setWindowPosition(glm::ivec2 position);
    void setWindowSize(int width, int height);
    void setWindowTitle(std::string title);

    void runEventLoop();

  private:

    void initGlState();
    void initGlutCallbacks();
    void createVertexBuffer(std::vector<glm::vec3> const & vertices);
    void createIndexBuffer(std::vector<unsigned int> const & indices);
    void compileShaders();

    glm::mat4 getViewProjectionMat();

    // OMG, hack for C callback, maybe it causes problems on some machine!
    static void wrapperOnRenderScene();
    static void wrapperOnMoveMouse(int x, int);
    static void wrapperOnMouse(int button, int state, int x, int y);
    static void wrapperOnReshapeWindow(int width, int height);
    static void wrapperOnKeyboard(unsigned char key, int, int);
    static void wrapperOnPopupMenu(int option);

    void onRenderScene();
    void onMoveMouse(int x, int);
    void onMouse(int button, int state, int x, int y);
    void onReshapeWindow(int width, int height);
    void onKeyboard(unsigned char key, int, int);
    void onPopupMenu(int option);


  private:
    MainWindow(int argc = 0, char ** argv = 0);


    MainWindow(MainWindow const &);
    MainWindow const & operator=(MainWindow const &);

    GLfloat _width;
    GLfloat _height;
    int _posX;
    int _posY;
    // other attributes
    std::string _title;
    // camera
    GLfloat _fov;
    GLfloat _heading;
    GLfloat _pitch;
    GLfloat _zNear;
    GLfloat _zFar;
    // uniforms
    GLint typeColoring;
    GLint gModelMVP;
    GLint gMeshMVP;
    GLint gTypeColoring;
    GLint gNear;
    GLint gFar;
    // other
    bool onMesh;
    GLsizei _sizeIndices;
    // handles GL
    GLuint VBO;
    GLuint IBO;
    GLuint programModel;
    GLuint programMesh;

    // hint for C callback
    static MainWindow* _instance;

  };

}

#endif //__MainWindow_H_
