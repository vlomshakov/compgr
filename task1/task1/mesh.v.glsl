#version 120

attribute vec3 Position;

uniform mat4 gMVP;

void main()
{
  gl_Position = gMVP * vec4(Position, 1.0);
}