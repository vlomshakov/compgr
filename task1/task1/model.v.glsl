#version 120

attribute vec3 Position;

uniform mat4 gMVP;

uniform float near;
uniform float far;

varying float zVar;

void main()
{
  vec4 vertexEyeSpace = gMVP * vec4(Position, 1.0);

  zVar = (vertexEyeSpace.w - near) / (far - near);

  gl_Position = vertexEyeSpace;
}

