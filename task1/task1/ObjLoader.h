//
// Created by Vadim Lomshakov on 10/15/13.
// Copyright (c) 2013 lomshakov. All rights reserved.
//
// To change the template use AppCode | Preferences | File Templates.
//



#ifndef __ObjLoader_H_
#define __ObjLoader_H_

#include <iostream>
#include <vector>
#include "vgl.h"

namespace graphics {

  class ObjLoader {
  public:

    /// Parses simple obj file
    /// throws std::ifstream::failure ; if io operation is failed
    static void loadObjFile(char const *path, std::vector<glm::vec3>& outVer, std::vector<unsigned int>& outVerIndices);

  private:
    static char const * const VERT_PREFIX;
    static char const * const UV_PREFIX;
    static char const * const NORMAL_PREFIX;
    static char const * const FACE_PREFIX;
  };


}// graphics


#endif //__ObjLoader_H_
